import numpy as np

a = 123
b = 321
print('a * b = ', a * b)

vector1 = np.array([3, 8, 9, 10, 12])
vector2 = np.array([8, 7, 7, 5, 6])

print('Suma wektorów: ', vector1 + vector2)
print('Iloczyn wektorów: ', vector1 * vector2)
print('Iloczyn skalarny: ', vector1.dot(vector2))
print('Długość euklidesowa: ', np.sqrt(np.sum((vector1-vector2) ** 2)))

macierz1 = np.array([[1,2,3],[3,4,5],[5,6,7]])
macierz2 = np.array([[7,8,9],[9,1,2],[2,3,4]])

print('Mnożenie macierzy: ', macierz1 * macierz2)
print('Mnożenie macierzowe: ', macierz1@macierz2)

vector3 = np.array(np.random.randint(1, 100, 50))
print('vector3: ', vector3)
print('Średnia: ', np.mean(vector3))
v3min = np.min(vector3)
v3max = np.max(vector3)
print('Min: ', v3min)
print('Max: ', v3max)
print('Odchylenie standardowe: ', np.std(vector3))

vector3norm = []
for i in vector3:
    vector3norm.append(float("{:.2f}".format((i - v3min)/(v3max-v3min))))

print('Normalizacja', vector3norm)
v3normMax = np.max(vector3norm)
print('Max', v3normMax)
print('Pozycja1: ', np.where(vector3 == v3max))
print('Pozycja2: ', np.where(vector3norm == v3normMax))