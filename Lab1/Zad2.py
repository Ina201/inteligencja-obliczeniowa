import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

miasta = pd.read_csv("miasta.csv")

data = {
    'Rok': ['2010'],
    'Gdansk': ['460'],
    'Poznan': ['555'],
    'Szczecin': ['405']
}
df = pd.DataFrame(data)

df.to_csv('miasta.csv', mode='a', index=False, header=False)

xpoints = np.array(miasta['Rok'])
y1 = np.array(miasta['Gdansk'])
y2 = np.array(miasta['Poznan'])
y3 = np.array(miasta['Szczecin'])

plt.title("Ludnosc w miastach Polski",)
plt.xlabel("Lata",)
plt.ylabel("Liczba ludności [w tys.]",)

plt.plot(xpoints, y1, marker = 'o', color = 'r', label = "Gdansk")
plt.plot(xpoints, y2, marker = 'o', color = 'c', label = "Poznan")
plt.plot(xpoints, y3, marker = 'o', color = 'y', label = "Szczecin")

plt.legend(title = "Miasta")
plt.show()

