import pygad
import numpy

S = [[2, 0, 0, 1, 0, 0, 0, 1, 0, 0],
     [1, 1, 0, 0, 0, 1, 0, 1, 1, 0],
     [0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
     [0, 1, 0, 1, 1, 0, 0, 1, 1, 0],
     [0, 0, 1, 1, 0, 0, 0, 1, 0, 0],
     [0, 0, 0, 0, 0, 1, 0, 0, 0, 1],
     [0, 1, 0, 0, 1, 1, 0, 1, 0, 0],
     [0, 1, 1, 1, 0, 0, 0, 1, 1, 0],
     [0, 1, 0, 1, 1, 0, 1, 0, 1, 0],
     [0, 1, 0, 0, 0, 0, 0, 0, 0, 3]]

max_steps = 30
def distance(x1, x_exit, y1, y_exit):
    return -(numpy.sqrt((x1-x_exit)**2+(y1-y_exit)**2))


#definiujemy parametry chromosomu
#geny to liczby: 0 lub 1
gene_space = [0, 1, 2, 3]
# (L, P, G, D)

#definiujemy funkcję fitness
def fitness_func(solution, solution_idx):
    x1 = 0
    y1 = 0
    x_exit = 9
    y_exit = 9
    for i in range (0, len(solution)):
        if x1 == 9 and y1 == 9:
            return 0
        if i == 30:
            return distance(x1, x_exit, y1, y_exit)
        current_step = solution[i]
        if current_step == 0:
            tmp_x1 = x1-1
            if tmp_x1 > 0 and S[y1][tmp_x1] == 0:
                x1 = tmp_x1
            else:
                return distance(x1, x_exit, y1, y_exit)
        elif current_step == 1:
            tmp_x1 = x1+1
            if tmp_x1 < 10 and S[y1][tmp_x1] == 0:
                x1 = tmp_x1
            else:
                return distance(x1, x_exit, y1, y_exit)
        elif current_step == 2:
            tmp_y1 = y1-1
            if tmp_y1 > 0 and S[tmp_y1][x1] == 0:
                y1 = tmp_y1
            else:
                return distance(x1, x_exit, y1, y_exit)
        elif current_step == 3:
            tmp_y1 = y1+1
            if tmp_y1 < 10 and S[tmp_y1][x1] == 0:
                y1 = tmp_y1
            else:
                return distance(x1, x_exit, y1, y_exit)


fitness_function = fitness_func

#ile chromsomów w populacji
#ile genow ma chromosom
sol_per_pop = 50
num_genes = max_steps

#ile wylaniamy rodzicow do "rozmanazania" (okolo 50% populacji)
#ile pokolen
#ilu rodzicow zachowac (kilka procent)
num_parents_mating = 25
num_generations = 100
keep_parents = 2

#jaki typ selekcji rodzicow?
#sss = steady, rws=roulette, rank = rankingowa, tournament = turniejowa
parent_selection_type = "sss"

#w il =u punktach robic krzyzowanie?
crossover_type = "single_point"

#mutacja ma dzialac na ilu procent genow?
#trzeba pamietac ile genow ma chromosom
mutation_type = "random"
mutation_percent_genes = 8

#inicjacja algorytmu z powyzszymi parametrami wpisanymi w atrybuty
ga_instance = pygad.GA(gene_space=gene_space,
                       num_generations=num_generations,
                       num_parents_mating=num_parents_mating,
                       fitness_func=fitness_function,
                       sol_per_pop=sol_per_pop,
                       num_genes=num_genes,
                       parent_selection_type=parent_selection_type,
                       keep_parents=keep_parents,
                       crossover_type=crossover_type,
                       mutation_type=mutation_type,
                       mutation_percent_genes=mutation_percent_genes)

#uruchomienie algorytmu
ga_instance.run()

#podsumowanie: najlepsze znalezione rozwiazanie (chromosom+ocena)
solution, solution_fitness, solution_idx = ga_instance.best_solution()
print("Parameters of the best solution : {solution}".format(solution=solution))
print("Fitness value of the best solution = {solution_fitness}".format(solution_fitness=solution_fitness))

#tutaj dodatkowo wyswietlamy sume wskazana przez jedynki
#prediction = numpy.sum(S*solution)
#print("Predicted output based on the best solution : {prediction}".format(prediction=prediction))

#wyswietlenie wykresu: jak zmieniala sie ocena na przestrzeni pokolen
ga_instance.plot_fitness()