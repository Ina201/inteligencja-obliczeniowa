import pandas as pd
import numpy as np

df = pd.read_csv("iris_with_errors.csv")
print(df.head())

missing_values = ["n/a", "na", "-"]
df = pd.read_csv("iris_with_errors.csv", na_values = missing_values)

print(df.isnull().sum())

median_sepal_l = df['sepal.length'].median()
median_sepal_w = df['sepal.width'].median()
median_petal_l = df['petal.length'].median()
median_petal_w = df['petal.width'].median()

# df.loc[df['sepal.length'] < 0, df['sepal.length']] = median_sepal_l
df['sepal.length'][df['sepal.length'] < 0] = median_sepal_l
df['sepal.length'][df['sepal.length'] > 15] = median_sepal_l
df['sepal.width'][df['sepal.width'] < 0] = median_sepal_w
df['sepal.width'][df['sepal.width'] > 15] = median_sepal_w
df['petal.length'][df['petal.length'] < 0] = median_petal_l
df['petal.length'][df['petal.length'] > 15] = median_petal_l
df['petal.width'][df['petal.width'] < 0] = median_petal_w
df['petal.width'][df['petal.width'] > 15] = median_petal_w

print(df.head())

variety = df['variety']
setosa = variety.str.contains('Setosa')
versicolor = variety.str.contains('Versicolor')
virginica = variety.str.contains('Virginica')

df['variety'] = np.where(setosa, 'Setosa',
                            np.where(versicolor, 'Versicolor',
                                     np.where(virginica, 'Virginica',
                                              variety.str.replace('-', ' '))))
print(df.head())